import urllib.request
import re
import random
import pymysql
import time

''' 链家网深圳租房信息爬取 '''

if __name__ == '__main__':

    user_agent_pools = [
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',
        'Mozilla/5.0(Macintosh;IntelMacOSX10.6;rv:2.0.1)Gecko/20100101Firefox/4.0.1',
        'Opera/9.80(WindowsNT6.1;U;en)Presto/2.8.131Version/11.11',
        'Mozilla/4.0(compatible;MSIE7.0;WindowsNT5.1;Trident/4.0;SE2.XMetaSr1.0;SE2.XMetaSr1.0;.NETCLR2.0.50727;SE2.XMetaSr1.0)'
    ]

    def chooseUA(user_agent_pools):
        user_agent = random.choice(user_agent_pools)
        headers = ('User-Agent', user_agent)
        opener = urllib.request.build_opener()
        opener.addheaders = [headers]
        urllib.request.install_opener(opener)

    print('开始连接数据库……')

    db = pymysql.Connect(host="127.0.0.1", user="root", password="123456", db="feature_ai", port=3306)

    cur = db.cursor()

    print('连接数据库成功……')

    url_pat = '<div class="pic-panel"><a target="_blank" href="(.*?)">'

    for i in range(1, 101):


        chooseUA(user_agent_pools)

        url = "https://sz.lianjia.com/zufang/pg" + str(i)

        data = urllib.request.urlopen(url).read().decode('utf-8')

        url_list = re.compile(url_pat).findall(data)

        for detail_url in url_list:

            chooseUA(user_agent_pools)

            try:

                detail_data = urllib.request.urlopen(detail_url).read().decode('utf-8')

                title = re.compile('<h1 class="main">(.*?)</h1>').findall(detail_data)[0]

                description = re.compile('<div class="sub">(.*?)</div>').findall(detail_data)[0]

                price = re.compile('<span class="total">(.*?)</span>').findall(detail_data)[0]

                acreage = re.compile('<p class="lf"><i>面积：</i>(.*?)平米.*?</p>').findall(detail_data)[0]

                type = re.compile('<p class="lf"><i>房屋户型：</i>(.*?)</p>').findall(detail_data)[0]

                position = re.compile('<p><i>位置：</i><a href=".*?">(.*?)</a>').findall(detail_data)[0]

                high = re.compile('<p class="lf"><i>楼层：</i>(.*?)楼层.*?</p>').findall(detail_data)[0]

                direct = re.compile('<p class="lf"><i>房屋朝向：</i>(.*?)</p>').findall(detail_data)[0]

                sql = "INSERT INTO lianjia_shenzhen_crawl_data(title, descri, price, acreage, position, type, high, direc) VALUES ('" + title + "','" + description + "','" + price + "','" + acreage + "','" + position + "','" + type + "','" + high + "','" + direct + "')"

                print(sql)

                cur.execute(sql)

                db.commit()

                time.sleep(0.2)
            except Exception as e:

                print('该地址爬取失败 ：' + e)