import re
import urllib.request
import io
import gzip

''' CSDN博客首页爬取 '''

if __name__ == '__main__':

    url = 'https://blog.csdn.net/'

    user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'

    request = urllib.request.Request(url)

    request.add_header('User_Agent', user_agent)

    data = urllib.request.urlopen(request).read().decode('utf-8', 'ignore')

    pattern = '<a href="(.*?)" target="_blank" data-track-click='

    links = list(set(re.compile(pattern).findall(data)))

    print('共爬取[' + str(len(links)) + ']个URL地址')

    path = '/Users/oldsix/workspace/python/featureai-crawl/csdn_blog_index/data/'

    for i in range(0, len(links)):
        link = links[i]
        try:
            urllib.request.urlretrieve(link, path + str(i + 1) + '.html')
            print('URL:['+link+']爬取成功!')
        except Exception as e:
            print('URL:['+link+']爬取异常!')