import urllib.request as ur
import re

if __name__ == '__main__':

    keyword = "吴德金"

    keyword = ur.quote(keyword)

    file = open('/Users/oldsix/workspace/python/featureai-crawl/urllib/urllib_example_baidu.txt','a')

    for i in range(1, 11):

        url = 'http://www.baidu.com/s?wd=' + keyword + '&pn=' + str((i - 1) * 10)

        print('当前页数：[' + str(i) + '], 开始爬取……')

        try:
            data = ur.urlopen(url, timeout=5).read().decode('utf-8')

            pattern1 = '"title":"(.*?)",'

            pattern2 = "title:'(.*?)',"

            result1 = re.compile(pattern1).findall(data)

            result2 = re.compile(pattern2).findall(data)

            for i in range(0, len(result1)):
                file.write(result1[i] + '\n')

            for j in range(0, len(result2)):
                file.write(result2[j] + '\n')
        except Exception as err:
            print('出现异常:' + str(err))

    file.close()

    ur.urlcleanup()