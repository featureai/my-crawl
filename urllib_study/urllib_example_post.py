import urllib.parse
import urllib.request

if __name__ == '__main__':

    url = 'http://localhost:8080/crawl/post'

    ''' 通过parse设置post参数 '''
    param = urllib.parse.urlencode({'name':'oldsix','pass':'666666',}).encode('utf-8')

    ''' Request(url, param) 进行post请求 '''
    request = urllib.request.Request(url, param)

    result = urllib.request.urlopen(request).read().decode('utf-8')

    file = open('/Users/oldsix/workspace/python/featureai-crawl/urllib/urllib_example_post.html', 'w+')

    file.write(result)

    file.close()