import urllib.request
import re

if __name__ == '__main__':

    url = 'https://read.douban.com/provider/all'

    print('开始爬取……')

    data = urllib.request.urlopen(url).read().decode('utf-8')

    print('爬取成功……')

    name_pattern = '<div class="name">(.*?)</div>'

    number_pattern = '<div class="works-num">(\d*?) 部作品在售</div>'

    print('提取数据……')

    name_list = re.compile(name_pattern).findall(data)

    number_list = re.compile(number_pattern).findall(data)

    print('写入文件……')

    file = open('/Users/oldsix/workspace/python/featureai-crawl/urllib/urllib_example_1.txt','w+')

    file.write("出版社名称\t\t\t\t\t作品在售数量\n" )

    count = 0

    for word in name_list:
        number = number_list[count]
        file.write(word + '\t\t\t\t\t' + number + '\n')
        count += 1

    file.close()

    print('写入成功!')