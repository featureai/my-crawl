import urllib.request as ur
import urllib.error as ue

''' 
    异常处理
    HTTPError：原因、状态码
    URLError：原因
'''

if __name__ == '__main__':

    try:
        ur.urlopen('http://blog.csdn.net', timeout=5)
    except ue.URLError as e:
        if hasattr(e, 'code'):
            print(e.code)
        if hasattr(e, 'reason'):
            print(e.reason)