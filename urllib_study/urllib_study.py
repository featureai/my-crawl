import urllib.request as ur

if __name__ == '__main__':

    ''' urlretrieve(网址, 本地文件存储地址) 直接下载网页到本地 '''

    url = 'http://www.baidu.com'

    path = '/Users/oldsix/workspace/python/featureai-crawl/urllib/urlretrieve_test.html'

    ur.urlretrieve(url, path)

    ''' urlcleanup() 清除缓存 '''

    ur.urlcleanup()

    file = ur.urlopen(url)

    ''' info() 获取网页相关的简介信息 '''

    print(file.info())

    ''' getcode() 获取爬取网页返回的状态码 '''

    print(file.getcode())

    ''' geturl() 获取当前访问的网页url '''

    print(file.geturl())

    ur.urlcleanup()

    ''' 爬取时超时处理 '''

    for i in range(0, 100):
        try:
            file = ur.urlopen(url, timeout=.3)
            print(len(file.read().decode('utf-8')))
        except Exception as err:
            print('出现异常:' + str(err))
