import urllib.request
import re
import random
import pymysql

''' 淘宝商品信息爬取 '''

if __name__ == '__main__':

    '''
        raw_title : 标题
        view_price : 价格
        item_loc : 产地
        view_sales : 销售量
        comment_count : 评论数
    '''

    user_agent_pools = [
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',
        'Mozilla/5.0(Macintosh;IntelMacOSX10.6;rv:2.0.1)Gecko/20100101Firefox/4.0.1',
        'Opera/9.80(WindowsNT6.1;U;en)Presto/2.8.131Version/11.11',
        'Mozilla/4.0(compatible;MSIE7.0;WindowsNT5.1;Trident/4.0;SE2.XMetaSr1.0;SE2.XMetaSr1.0;.NETCLR2.0.50727;SE2.XMetaSr1.0)'
    ]

    def chooseUA(user_agent_pools):
        user_agent = random.choice(user_agent_pools)
        print(user_agent)
        headers = ('User-Agent', user_agent)
        opener = urllib.request.build_opener()
        opener.addheaders = [headers]
        urllib.request.install_opener(opener)

    db = pymysql.Connect(host="127.0.0.1", user="root", password="123456", db="feature_ai", port=3306)

    cur = db.cursor()

    param = '床'

    keyword = urllib.request.quote(param)

    title_pat = '"raw_title":"(.*?)"'

    price_pat = '"view_price":"(.*?)"'

    location_pat = '"item_loc":"(.*?)"'

    sale_pat = '"view_sales":"(.*?)"'

    comment_pat = '"comment_count":"(.*?)"'

    for i in range(1, 101):
        url = 'https://s.taobao.com/search?q=' + keyword + '&s=' + str((i - 1) * 44) + '&sort=sale-desc'

        print('开始爬取第[' + str(i) + ']页……')

        data = urllib.request.urlopen(url).read().decode('utf-8', 'ignore')

        title_list = re.compile(title_pat).findall(data)

        print('第[' + str(i) + ']页共获取到[' + str(len(title_list)) + ']个标题')

        price_list = re.compile(price_pat).findall(data)

        print('第[' + str(i) + ']页共获取到[' + str(len(price_list)) + ']个价格')

        location_list = re.compile(location_pat).findall(data)

        print('第[' + str(i) + ']页共获取到[' + str(len(location_list)) + ']个产地')

        sale_list = re.compile(sale_pat).findall(data)

        print('第[' + str(i) + ']页共获取到[' + str(len(sale_list)) + ']个销售量')

        comment_list = re.compile(comment_pat).findall(data)

        print('第[' + str(i) + ']页共获取到[' + str(len(comment_list)) + ']个评论数')

        is_equal = (len(title_list) + len(price_list) + len(location_list) + len(sale_list) + len(comment_list)) - 5 * len(title_list)

        if is_equal == 0:

            for i in range(0, len(title_list)):

                print('开始插入数据库……')

                sql = "insert into taobao_crawl_data(raw_title, view_price, item_loc, view_sales, comment_count) values ('" + title_list[i] + "','" + price_list[i] + "','" + location_list[i] + "','" + sale_list[i][:-3] + "','" + comment_list[i] + "')"

                print(sql)

                cur.execute(sql)

                db.commit()

    db.close()