import re
import urllib.request
import random

''' 淘宝商品图片爬取 '''

if __name__ == '__main__':

    user_agent_pools = [
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',
        'Mozilla/5.0(Macintosh;IntelMacOSX10.6;rv:2.0.1)Gecko/20100101Firefox/4.0.1',
        'Opera/9.80(WindowsNT6.1;U;en)Presto/2.8.131Version/11.11',
        'Mozilla/4.0(compatible;MSIE7.0;WindowsNT5.1;Trident/4.0;SE2.XMetaSr1.0;SE2.XMetaSr1.0;.NETCLR2.0.50727;SE2.XMetaSr1.0)'
    ]


    def chooseUA(user_agent_pools):
        user_agent = random.choice(user_agent_pools)
        print(user_agent)
        headers = ('User-Agent', user_agent)
        opener = urllib.request.build_opener()
        opener.addheaders = [headers]
        urllib.request.install_opener(opener)

    param = '床'

    keyword = urllib.request.quote(param)

    pattern = '"pic_url":"//(.*?)"'

    path = '/Users/oldsix/workspace/python/featureai-crawl/taobao_pic_crawl/data/' + param + '/'

    for i in range(1, 10):

        url = 'https://s.taobao.com/search?q=' + keyword + '&s=' + str((i - 1) * 44) + '&sort=sale-desc'

        data = urllib.request.urlopen(url).read().decode('utf-8', 'ignore')

        img_url_list = re.compile(pattern).findall(data)

        for j in range(0, len(img_url_list)):

            img_url = 'http://' + img_url_list[j]

            new_path = path + str(i) + '_' + str(j) + '.jpg'

            urllib.request.urlretrieve(img_url, new_path)

            print(str(i) + '_' + str(j) + '图片保存成功!')





