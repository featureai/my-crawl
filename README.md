# my-crawl

#### 项目介绍
   各种网站爬虫，只为爬取数据进行数据分析

#### 例子

1. [斗鱼直播爬虫之直播人数与观看热度爬取](https://gitee.com/featureai/my-crawl/blob/master/douyu_crawl/douyu_crawl.py)
    - [爬虫逻辑与代码分析](https://blog.csdn.net/king_old_six/article/details/81813904)
    - [数据分析与可视化逻辑分析]()

2. [链家网爬虫之深圳地区租房信息爬取](https://gitee.com/featureai/my-crawl/blob/master/lianjia_crawl/lianjia_sz_crawl.py)
3. [淘宝网爬虫之商品信息爬取](https://gitee.com/featureai/my-crawl/blob/master/taobao_crawl/taobao_crawl.py)
4. [QQ音乐爬虫之歌曲评论爬取](https://gitee.com/featureai/my-crawl/blob/master/qqmusic_crawl/qqmusic_crawl.py)
5. [淘宝网爬取之商品图片爬取](https://gitee.com/featureai/my-crawl/blob/master/taobao_pic_crawl/taobao_pic_crawl.py)
6. [糗事百科之文字段子爬取](https://gitee.com/featureai/my-crawl/blob/master/qiushibaike_text/qiushibaike_text_crawl.py)