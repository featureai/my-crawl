import re
import urllib.request
import random

''' 糗事百科文本段子爬取 '''

if __name__ == '__main__':

    url = 'https://www.qiushibaike.com/text/page/'

    user_agent_pools = [
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',
        'Mozilla/5.0(Macintosh;IntelMacOSX10.6;rv:2.0.1)Gecko/20100101Firefox/4.0.1',
        'Opera/9.80(WindowsNT6.1;U;en)Presto/2.8.131Version/11.11',
        'Mozilla/4.0(compatible;MSIE7.0;WindowsNT5.1;Trident/4.0;SE2.XMetaSr1.0;SE2.XMetaSr1.0;.NETCLR2.0.50727;SE2.XMetaSr1.0)'
    ]

    def chooseUA(user_agent_pools):
        user_agent = random.choice(user_agent_pools)
        print(user_agent)
        headers = ('User-Agent', user_agent)
        opener = urllib.request.build_opener()
        opener.addheaders = [headers]
        urllib.request.install_opener(opener)

    path = '/Users/oldsix/workspace/python/featureai-crawl/qiushibaike_text/qsbk.txt'

    file = open(path, 'w+')

    for i in range(1, 13):
        chooseUA(user_agent_pools)
        try:
            link = url + str(i) + '/'
            print('开始爬取第[' + str(i) + ']页')
            data = urllib.request.urlopen(url).read().decode('utf-8', 'ignore')
            pattern = '<div class="content">.*?<span>(.*?)</span>.*?</div>'
            result = re.compile(pattern, re.S).findall(data)
            for j in range(0, len(result)):
                file.write(result[j])
        except Exception as e:
            print('第[' + str(i) + ']页爬取失败')
    file.close()



