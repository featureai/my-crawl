import urllib.request
import re
import random
import pymysql
import time

''' QQ音乐 蔡依林新歌-《我对我》评论爬取 '''

if __name__ == '__main__':

    user_agent_pools = [
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',
        'Mozilla/5.0(Macintosh;IntelMacOSX10.6;rv:2.0.1)Gecko/20100101Firefox/4.0.1',
        'Opera/9.80(WindowsNT6.1;U;en)Presto/2.8.131Version/11.11',
        'Mozilla/4.0(compatible;MSIE7.0;WindowsNT5.1;Trident/4.0;SE2.XMetaSr1.0;SE2.XMetaSr1.0;.NETCLR2.0.50727;SE2.XMetaSr1.0)'
    ]

    def chooseUA(user_agent_pools):
        user_agent = random.choice(user_agent_pools)
        print(user_agent)
        headers = ('User-Agent', user_agent)
        opener = urllib.request.build_opener()
        opener.addheaders = [headers]
        urllib.request.install_opener(opener)

    print('开始连接数据库……')

    db = pymysql.Connect(host="127.0.0.1", user="root", password="123456", db="feature_ai", port=3306)

    cur = db.cursor()

    print('连接数据库成功……')

    comment_pat = '"rootcommentcontent" : "(.*?)"'

    time_pat = '"time" : (.*?),'

    praise_pat = '"praisenum" : (.*?),'

    for i in range(1, 556):
        chooseUA(user_agent_pools)

        print('开始爬取第[' + str(i) + ']页评论……')

        url = 'https://c.y.qq.com/base/fcgi-bin/fcg_global_comment_h5.fcg?g_tk=619472473&jsonpCallback=jsoncallback3964648577659764&loginUin=896819422&hostUin=0&format=jsonp&inCharset=utf8&outCharset=GB2312&notice=0&platform=yqq&needNewCode=0&cid=205360772&reqtype=2&biztype=1&topid=214083990&cmd=8&needmusiccrit=0&pagenum=' + str(i) + '&pagesize=25&lasthotcommentid=song_214083990_123808674_1531099893&callback=jsoncallback3964648577659764&domain=qq.com&ct=24&cv=101010'

        data = urllib.request.urlopen(url).read().decode('utf-8', 'ignore')

        comment_list = re.compile(comment_pat).findall(data)

        print('第['+ str(i) + ']共获取到[' + str(len(comment_list)) + ']条评论内容数据')

        time_list = re.compile(time_pat).findall(data)

        print('第['+ str(i) + ']共获取到[' + str(len(time_list)) + ']条评论时间数据')

        praise_list = re.compile(praise_pat).findall(data)

        print('第['+ str(i) + ']共获取到[' + str(len(praise_list)) + ']条评论获赞数据')

        if len(comment_list) == len(time_list) == len(praise_list):

            for i in range(0, len(comment_list)):

                print('开始插入数据库……')

                sql = "INSERT INTO qqmusic_crawl_data(comment, praisenum, comment_time) VALUES ('"+ comment_list[i].replace('\\', '').replace("'", "") + "','" + praise_list[i] + "','" + time_list[i] +"')"

                print(sql)

                try:
                    cur.execute(sql)
                except Exception as e:
                    print(e)

                db.commit()
        # time.sleep(3)