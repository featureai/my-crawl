import urllib.request
import re
import gzip
import io

'''
    腾讯新闻首页爬取
'''

if __name__ == '__main__':

    url = 'http://news.qq.com/'

    request = urllib.request.Request(url)

    ''' 添加浏览器伪装 '''
    request.add_header('User-Agent', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36')

    data = urllib.request.urlopen(request)

    ''' 获取Content-Encoding '''
    encoding = data.getheader('Content-Encoding')

    print(encoding)

    content = data.read()

    ''' 解压gzip '''
    if encoding == 'gzip':
        buff = io.BytesIO(content)
        gf = gzip.GzipFile(fileobj=buff)
        content = gf.read()

    ''' 设置编码 '''
    data = content.decode('gb2312', 'ignore')

    pattern1 = '<a target="_blank" class="linkto" href="(.*?)">'

    pattern2 = '<a target="_blank" href="(.*?)">'

    all_link = re.compile(pattern1).findall(data) + re.compile(pattern2).findall(data)

    ''' URL去重 '''
    all_link = list(set(all_link))

    print('共获取到[' + str(len(all_link)) + ']个URL地址')

    path = '/Users/oldsix/workspace/python/featureai-crawl/tencent_news/data/'

    for i in range(0, len(all_link)):

        try:
            this_link = all_link[i]
            this_page = urllib.request.urlretrieve(str(this_link), path + str(i + 1) + '.html')
            print('URL:[ '+ this_link + '] 爬取完成！')
        except Exception as e:
            print('URL:[ ' + this_link + '] 爬取失败！')
