import re
import urllib.request
import random

''' 搜狗微信文章爬取 '''

if __name__ == '__main__':

    user_agent_pools = [
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',
        'Mozilla/5.0(Macintosh;IntelMacOSX10.6;rv:2.0.1)Gecko/20100101Firefox/4.0.1',
        'Opera/9.80(WindowsNT6.1;U;en)Presto/2.8.131Version/11.11',
        'Mozilla/4.0(compatible;MSIE7.0;WindowsNT5.1;Trident/4.0;SE2.XMetaSr1.0;SE2.XMetaSr1.0;.NETCLR2.0.50727;SE2.XMetaSr1.0)'
    ]

    def chooseUA(user_agent_pools):
        user_agent = random.choice(user_agent_pools)
        headers = ('User-Agent', user_agent)
        opener = urllib.request.build_opener()
        opener.addheaders = [headers]
        urllib.request.install_opener(opener)

    param = 'spark源码分析'

    keyword = urllib.request.quote(param)

    pattern = '<div class="txt-box">.*?href="(.*?)"'

    url = 'http://weixin.sogou.com/weixin?query=' + keyword + '&type=2&page='

    for i in range(1, 11):

        chooseUA(user_agent_pools)

        this_url = url + str(i)

        data = urllib.request.urlopen(this_url).read().decode('utf-8', 'ignore')

        article_url_list = re.compile(pattern, re.S).findall(data)

        print('第[' + str(i) + ']页共获取[' + str(len(article_url_list)) + ']个文章url.')

        if len(article_url_list) > 0:

            for j in range(0, len(article_url_list)):

                path = '/Users/oldsix/workspace/python/featureai-crawl/sougou_weixin/data/' + str(i) + '_' + str(j) + '.html'

                urllib.request.urlretrieve(article_url_list[j], path)